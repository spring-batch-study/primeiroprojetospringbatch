package com.udemy.primeiroprojetospringbatch.step;

import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ImprimeOlaStepConfig {

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    public Step imprimeOlaStep(Tasklet imprimeOlaTasklet) {
        // para tarefas pequenas (tasklet)
        return this.stepBuilderFactory
                .get("imprimeOlaStep")
                .tasklet(imprimeOlaTasklet)
                .build();
    }

    // parametro nome foi adcionado em program arguments nome=Gardim
    /*@Bean
    @StepScope
    public Tasklet imprimeNome(@Value("#{jobParameters['nome']}") String nome) {
        return (stepContribution, chunkContext) -> {
            System.out.println(String.format("Olá, %s!", nome));
            return RepeatStatus.FINISHED;
        };
    }*/

}
